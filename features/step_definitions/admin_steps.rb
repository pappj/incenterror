When /^an admin is signed in$/ do
  email = "admin@admin.com"
  pwd = "admin-pwd12"
  Admin.create(email: email, password: pwd)
  visit(new_admin_session_path)
  fill_in "Email", with: email
  fill_in "Password", with: pwd
  click_button("Log in")
end

When /^visits the main admin page$/ do
  visit(admin_root_path)
end

When /^goes to partner manager$/ do
  click_link("Partner manager")
end

When /^visits the partner manager page of \"(.*)\"$/i do |partner_name|
  p = Partner.where({name: partner_name}).first
  id = if p
         p._id.to_s
       else
         ""
       end
  visit(partner_manager_path(id))
end