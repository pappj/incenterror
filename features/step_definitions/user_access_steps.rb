When /^\"(.*)\" fills the sign up form$/i do |username|
  click_button("Bejelentkezés Felhasználóként")
  click_button("Regisztráció")
  within("#user-registration-form") do
    fill_in "Email", with: username + "@gmail.com"
    fill_in "Username", with: username
    fill_in "Password", with: username + "-pwd"
    fill_in "Password confirmation", with: username + "-pwd"

    click_button("Regisztrálok")
  end
end

When /^\"(.*)\" fills the sign in form with (email|username)( incorrectly)?$/i do |username,method,incorrect|
  click_button("Bejelentkezés Felhasználóként")
  within("#user-login-form") do
    id = case method
           when "email"
             username + "@gmail.com"
           when "username"
             username
         end
    fill_in "Login", with: id
    if incorrect
      fill_in "Password", with: username + "-passwd"
    else
      fill_in "Password", with: username + "-pwd"
    end

    click_button("Bejelentkezés")
  end
end

When /^there is a user, \"(.*)\"$/i do |username|
  User.create(email: username + "@gmail.com", username: username, password: username + "-pwd")
end

When /^\"(.*)\" has just signed in$/i do |username|
  expect(page).to have_content(username)
  expect(find("#notice").text).not_to be_empty
end

When /^\"(.*)\" is signed up$/i do |username|
  expect(User.where(username: username).count).to eq(1)
end

When /^\"(.*)\" has been rejected from double sign up$/i do |username|
  expect(User.where(username: username).count).to eq(1)
  expect(page).to have_current_path(root_path)
  expect(page).not_to have_content("Sign out")
end

When /^no one is signed in$/ do
  expect(page).not_to have_content("Kijelentkezés")
end

When /^\"(.*?)\" is signed in$/ do |username|
  step "there is a user, \"#{username}\""
  step "he visits the home page"
  step "\"#{username}\" fills the sign in form with username"
end