When /^\"(.*?)\" event has \"(.*?)\" ratings/ do |event_title,rating_num|
  event = Event.find_by(title: event_title)
  rating_num.to_i.times do
    Rating.new(value: rand(5)+1, ratable: event).save
  end
end

When /^.*? sees the rating form$/ do
  expect(page).to have_selector("#rating-form")
end

When /^.*? fills the rating form with \"([1-5])\", \"(.*?)\"$/ do |rating, review|
  within("#rating-form") do
    find("label[for=rating_value_#{rating}]").click
    fill_in "rating-review", with: review
    click_button("Küldés")
  end
end

When /^the rating is \"(.*?)\" with \"(.*?)\" reviewer$/ do |rating, reviewers|
  elem = find("#rating")
  expect(elem).to have_content "#{rating}/5"
  expect(elem).to have_content "#{reviewers} értékelés"
end

When /^\"(.*?)\" has no opinion$/ do |username|
  expect(find("#rating-reviews")).not_to have_content username
end

When /^\"(.*?)\" has opinion \"(.*?)\"$/ do |username, opinion|
  reviews = find("#rating-reviews")
  expect(reviews).to have_content username
  expect(reviews).to have_content opinion
end