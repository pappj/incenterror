When /^.* visits the home page$/ do
  visit(root_path)
end

When /^there is an error$/ do
  expect(find("#error").text).not_to be_empty
end

When /^there is an error, \"(.*)\"$/ do |message|
  expect(find("#error").text).to eq(message)
end