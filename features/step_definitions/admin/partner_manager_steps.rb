When /^fills the select partner form of \"(.*)\"$/i do |partner_id|
  fill_in "Email or name:", with: partner_id
  click_button("Select partner")
end

When /^fills the new partner form for \"(.*)\"$/i do |partner_name|
  find(".panel-title", text: "Add new partner").click
  within("#add-partner") do
    fill_in "Name", with: partner_name
    fill_in "Email", with: partner_name.downcase + "@gmail.com"
    fill_in "Password", with: partner_name + "-pwd"
    fill_in "Password confirmation", with: partner_name + "-pwd"
    click_button("Register")
  end
end

When /^deselects the partner$/ do
  click_link("Deselect")
end

When /^sees the select partner form$/ do
  expect(page).to have_content "No partner selected. Select one below."
end

When /^sees \"(.*?)\" details$/i do |partner_name|
  expect(page).to have_content partner_name
  expect(page).to have_content "Deselect"
end

When /^doesn't see the select partner form$/ do
  expect(page).not_to have_content "No partner selected. Select one below."
  expect(page).not_to have_content "Select partner"
end