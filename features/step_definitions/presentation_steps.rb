When /^there is a presentation, \"(.*?)\"$/ do |title|
  partner = Partner.new(name: "MockJoe", email: "MockJoe@gmail.com", password: "123456", password_confirmation: "123456")
  partner.save
  Events::Presentation.new(title: title, partner: partner).save
end

When /^.*? visits \"(.*?)\" presentation page$/i do |title|
  event = Events::Presentation.find_by(title: title)
  visit(events_presentation_path(event))
end