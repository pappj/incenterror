When /^\"(.*)\" fills the partner sign in form( incorrectly)?$/i do |username,incorrect|
  click_button("Bejelentkezés Partnerként")
  within("#partner-login-form") do
    fill_in "Email", with: username + "@gmail.com"
    if incorrect
      fill_in "Password", with: username + "-passwd"
    else
      fill_in "Password", with: username + "-pwd"
    end

    click_button("Bejelentkezés")
  end
end

When /^there is a partner, \"(.*)\"$/i do |username|
  usr = Partner.create(email: username + "@gmail.com", name: username, password: username + "-pwd")
end