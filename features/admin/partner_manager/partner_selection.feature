Feature: Partner selection
  This feature tests the admin's partner manager's partner selection functionality through different scenarios.
  It assumes the admin has access and don't tests the case if the admin is not signed in.

  Scenario: Admin sees the select partner form
    Given an admin is signed in
    When visits the partner manager page of ""
    Then sees the select partner form

  Scenario: Admin can select a partner with name
    Given an admin is signed in
    And visits the partner manager page of ""
    And there is a partner, "george15"
    When fills the select partner form of "george15"
    Then sees "george15" details
    And doesn't see the select partner form

  Scenario: Admin can select a partner with email
    Given an admin is signed in
    And visits the partner manager page of ""
    And there is a partner, "george15"
    When fills the select partner form of "george15@gmail.com"
    Then sees "george15" details
    And doesn't see the select partner form
    
  Scenario: The selected partner is remembered
    Given there is a partner, "Sam12"
    And an admin is signed in
    And visits the partner manager page of "Sam12"
    When visits the main admin page
    And goes to partner manager
    Then sees "Sam12" details
    And doesn't see the select partner form

  Scenario: Admin gets error if the pertner id is wrong
    Given an admin is signed in
    And visits the partner manager page of ""
    When fills the select partner form of "stg_22"
    Then there is an error, "There is no such partner."
    And sees the select partner form

  Scenario: Admin can deselect the partner
    Given there is a partner, "BuTTn_25"
    And an admin is signed in
    And visits the partner manager page of "BuTTn_25"
    When deselects the partner
    Then sees the select partner form