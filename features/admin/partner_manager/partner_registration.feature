Feature: Partner registration
  This feature tests that an admin can register a partner.

  Scenario: Admin can register a new partner
    Given an admin is signed in
    And visits the partner manager page of ""
    When fills the new partner form for "Billy17"
    Then sees "Billy17" details
    And there is a partner, "Billy17"
    
  Scenario: A already existing partner cannot be registered twice
    Given there is a partner, "angElin4"
    And an admin is signed in
    And visits the partner manager page of ""
    When fills the new partner form for "angElin4"
    Then there is an error, "angElin4 is already in use as name.angelin4@gmail.com is already in use as email."