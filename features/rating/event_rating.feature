Feature: Event rating
  This feature tests whether the user can rate events, and the expected javascript response runs properly.

  Scenario: The rating form is shown
    Given "james16" is signed in
    And there is a presentation, "Relationships 101"
    And "Relationships 101" event has "0" ratings
    When james16 visits "Relationships 101" presentation page
    Then he sees the rating form

  Scenario: The new rating gets shown upon submission
    Given "andy2" is signed in
    And there is a presentation, "Relationships 101"
    And andy2 visits "Relationships 101" presentation page
    When he fills the rating form with "4", ""
    Then the rating is "4.0" with "1" reviewer
    And "andy2" has no opinion

  Scenario: The review is shown on the page
    Given "catodog" is signed in
    And there is a presentation, "Relationships 101"
    And she visits "Relationships 101" presentation page
    When she fills the rating form with "2", "It was okay"
    Then "catodog" has opinion "It was okay"