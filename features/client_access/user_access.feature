Feature: User access
  This feature tests whether users can sign in and have (or not) the right access to the site

  Scenario: A user can sign up
    Given Bob visits the home page
    When "Bob" fills the sign up form
    Then "Bob" has just signed in
    And "Bob" is signed up

  Scenario: A user can't sign up twice
    Given there is a user, "philip16"
    And someone visits the home page
    When "philip16" fills the sign up form
    Then there is an error
    And "philip16" has been rejected from double sign up

  Scenario: A user can sign in with username
    Given there is a user, "James"
    And he visits the home page
    When "James" fills the sign in form with username
    Then "James" has just signed in

  Scenario: A user can sign in with email
    Given there is a user, "James"
    And he visits the home page
    When "James" fills the sign in form with email
    Then "James" has just signed in

  Scenario: A user gives wrong credentials
    Given there is a user, "Sarah"
    And she visits the home page
    When "Sarah" fills the sign in form with username incorrectly
    Then there is an error
    And no one is signed in