Feature: Partner access
  This feature tests whether partners can sign in and have (or not) the right access to the site

  Scenario: A partner can sign in with email
    Given there is a partner, "James"
    And he visits the home page
    When "James" fills the partner sign in form
    Then "James" has just signed in

  Scenario: A partner gives wrong credentials
    Given there is a partner, "Sarah"
    And she visits the home page
    When "Sarah" fills the partner sign in form incorrectly
    Then there is an error
    And no one is signed in