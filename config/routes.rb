Rails.application.routes.draw do
  devise_for :admins, path: "deadpool", path_names: {sign_in: "startTheFun"}, controllers: { sessions: "admin/sessions" }
  devise_for :partners, controllers: { registrations: "partner_controllers/registrations", sessions: "partner_controllers/sessions" }
  devise_for :users, controllers: { registrations: "user_controllers/registrations", sessions: "user_controllers/sessions" }

  resources :users, only: [:show, :edit]
  resources :partners, only: [:show, :edit]
  resources :events, only: [:index, :new, :destroy]
  resources :quests

  namespace :events do
    resources :presentations, except: [:index, :destroy]
    resources :courses, except: [:index, :destroy]

    namespace :courses do
      resources :presentations, only: [:show, :edit, :update]
    end
  end

  root "home#index"
  delete "logout", to: "home#logout"

  # User subpage aliases
  get "users/:id?sp=personal", to: "users#show", as: "user_personal"
  get "users/:id?sp=psychological", to: "users#show", as: "user_psychological"
  get "users/:id?sp=physical", to: "users#show", as: "user_physical"
  get "users/:id?sp=skill", to: "users#show", as: "user_skill"
  get "users/:id?sp=quest", to: "users#show", as: "user_quest"

  ##########
  # EVENTS #
  ##########
  
  # Occurrence subscription and payment
  patch "events/occurrences/:occurrence_id/subscription", to: "events/occurrences#subscription", as: "subscription"
  patch "events/occurrences/:occurrence_id/pay", to: "events/occurrences#pay", as: "pay_occurrence"
  
  # Confirm users individually.
  get "events/occurrences/:occurrence_id/checkin", to: "events/occurrences#checkin", as: "checkin"
  patch "events/occurrences/:occurrence_id/checkin/confirm_single_user", to: "events/occurrences#confirm_single_user", as: "confirm_single_user"
  patch "events/occurrences/:occurrence_id/checkin/undo", to: "events/occurrences#unconfirm", as: "undo_confirmation"

  # Confirm multiple users at once
  get "events/occurrences/:occurrence_id/checkin_list", to: "events/occurrences#checkin_list", as: "checkin_list"
  patch "events/occurrences/:occurrence_id/checkin_list", to: "events/occurrences#confirm_list", as: "confirm_list"

  # Finalize user confirmation
  patch "events/occurrences/:occurrence_id/checkin/finalize", to: "events/occurrences#finalize", as: "finaliza_occurrence"

  # Commenting
  post "events/:event_id/comment", to: "events#create_comment", as: "new_comment"
  delete "events/:event_id/comment/:comment_id", to: "events#destroy_comment", as: "destroy_comment"

  #########
  # ADMIN #
  #########

  get "deadpool/hq", to: "admin#show", as: "hq"
  get "deadpool/hq", to: "admin#show", as: "admin_root"
  
    # User Manager #
    get "deadpool/userManager", to: "admin/user_manager#show", as: "user_manager"
    post "deadpool/userManager/get", to: "admin/user_manager#get_user", as: "get_user"
    post "deadpool/userManager/adjust_credit", to: "admin/user_manager#adjust_credit", as: "adjust_credit"
  
    # Partner Manager #
    get "deadpool/partnerManager/(:id)", to: "admin/partner_manager#show", as: "partner_manager"
    post "deadpool/partnerManager/selectPartner", to: "admin/partner_manager#select_partner", as: "admin_select_partner"
    patch "deadpool/partnerManager/deselectPartner", to: "admin/partner_manager#deselect_partner", as: "admin_deselect_partner"
  
  
  get "deadpool/eventManager", to: "admin/event_manager#show", as: "event_manager"
  get "deadpool/questManager", to: "admin/quest_manager#show", as: "quest_manager"
  patch "deadpool/change_password", to: "admin#update_password", as: "change_admin_password"
  
  
  ##########
  # OTHERS #
  ##########

  # Quests
  #get "users/:id/quests", to: "users#show_quests", as: "user_quests"
  patch "users/:id/change_quest_visibility", to: "users#quest_visibility", as: "user_quest_visibility"
  patch "quests/:id/assignment", to: "quests#assignment", as: "quest_assignment"

  post "ratings/new", to: "ratings#create", as: "rating"

  ###########
  # TESTING #
  ###########
  
  # These are needed for rapid user/partner registration and changing them.
  post "test_tools/new_user", to: "test_tools#new_user", as: "test_new_user"
  post "test_tools/change_user", to: "test_tools#change_user", as: "test_change_user"
end
