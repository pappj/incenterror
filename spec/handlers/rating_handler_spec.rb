require 'rails_helper'

describe RatingHandler do
  def rating_array
    [FactoryGirl.create(:rating, value: 2),
     FactoryGirl.create(:rating, value: 4),
     FactoryGirl.create(:rating, value: 2),
     FactoryGirl.create(:rating, value: 5),]
  end

  describe "Calculates average rating" do
    before do
      rh = RatingHandler.new(rating_array)
      @ra = rh.calc_rating_aggregate
    end

    it "calculates the right average" do
      expect(@ra.value).to be 3.25
    end

    it "calculates the right percent" do
      expect(@ra.percent).to be 65.0
    end
  end

  describe "Caches calculated rating" do
    it "stores the data in hash" do
      rh = RatingHandler.new(rating_array)
      session = {}
      rh.calc_rating_aggregate(session)
      expect(session[:rating_aggregate_tvalue]).to be 13
      expect(session[:rating_aggregate_count]).to be 4
    end
  end

  describe "Adjusts rating" do
    context "The current rating is cached" do
      before do
        rh = RatingHandler.new(rating_array)
        cache = {rating_aggregate_tvalue: 42, rating_aggregate_count: 12}
        @ra = rh.load_from_session_and_calc_rating_aggregate(4, cache)
      end

      it "calculates the right average" do
        expect(@ra.value).to be 3.54
      end

      it "calculates the right percent" do
        expect(@ra.percent).to be 70.76923076923076
      end
    end

    context "The current rating is not in the cache" do
      before do
        rh = RatingHandler.new(rating_array)
        @ra = rh.load_from_session_and_calc_rating_aggregate(4, {})
      end

      it "calculates the right average" do
        expect(@ra.value).to be 3.4
      end

      it "calculates the right percent" do
        expect(@ra.percent).to be 68.0
      end
    end
  end
end