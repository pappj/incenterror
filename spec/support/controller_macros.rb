module ControllerMacros
  def login_user user = nil
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      @user = user.nil? ? FactoryGirl.create(:user) : user
      sign_in @user
    end
  end

  def login_partner
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:partner]
      @partner = FactoryGirl.create(:partner)
      sign_in @partner
    end
  end
end