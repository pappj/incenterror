require 'rails_helper'

describe UsersController do
  describe "PATCH #quest_visibility" do
    login_user

    it "doesn't change anything for not-signed in user" do
      user = FactoryGirl.create(:user)
      patch :quest_visibility, id: user, format: :js
      expect(user.quest_public).to eq(user.reload.quest_public)
    end

    it "toggles the logged_in user's quest visibility" do
      patch :quest_visibility, id: @user, format: :js
      expect(@user.quest_public).to eq(!@user.reload.quest_public)
    end
  end
end