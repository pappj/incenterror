require 'rails_helper'

describe Events::OccurrencesController do
  before(:each) do
		@event = FactoryGirl.create(:presentation, occ_count: 1)
		@occ = @event.occurrences.first
	end

	def participate status
		p = FactoryGirl.create(:participation, status: status,user: @user)
		@occ.participations << p
	end

	describe "PATCH #subscription" do
		login_user

		context "User isn't signed in" do
			it "should not change the occurrence" do
				sign_out :user
				patch :subscription, occurrence_id: @occ, format: :js
				expect(@occ).to eq(@occ.reload)
				expect(@occ.participations.length).to eq(0)
				expect(response).to render_template("subscription")
			end
		end

		context "User hasn't subscribed yet" do
			before do
				patch :subscription, occurrence_id: @occ, format: :js
			end
			
			it "creates a user subscription with going status" do
				expect(@occ.participations.length).to eq(1)
				expect(@occ.participations.first.status).to eq(:going)
				expect(@occ.participations.first.user).to eq(@user)
			end

			it "renders subscription.js" do
				expect(response).to render_template("subscription")
			end
		end

		context "User is going" do
			it "removes the user from participation list" do
				participate(:going)
				patch :subscription, occurrence_id: @occ, format: :js
				expect(@occ.reload.participations.length).to eq(0)
				expect(response).to render_template("subscription")
			end
		end

		context "User has already paid" do
			before do
				participate(:prepaid)
				@before_balance = @user.credit
			end

			it "removes the user from participation list" do
				patch :subscription, occurrence_id: @occ, format: :js
				expect(@occ.reload.participations.length).to eq(0)
				expect(response).to render_template("subscription")
			end

			it "pays back the full price one hour before the occurrence" do
				patch :subscription, occurrence_id: @occ, format: :js
				expect(@user.reload.credit).to eq(@before_balance + @event.price_in_credit)
			end

			it "pays back half the price less than one hour before the occurrence" do
				@occ.date = Time.now + 60*rand(1..50) # it starts in 50 minutes
				@occ.save
				patch :subscription, occurrence_id: @occ, format: :js
				expect(@user.reload.credit).to eq(@before_balance + @event.price_in_credit*0.5)
			end
		end
	end

	describe "PATCH #pay" do
		login_user

		RSpec.shared_examples "unchanged occurrence" do |expected_status|
			it "should not change the occurrence" do
				expect(@occ).to eq(@occ.reload)
				expect(@occ.participations.length).to eq(1)
				expect(@occ.participations.first.status).to eq(expected_status)
				expect(response).to render_template("subscription")
			end

			it "should not change user's credit balance" do
				expect(@user.reload.credit).to eq(@before_balance)
			end
		end

		context "User isn't signed in" do
			before do
				sign_out :user
				participate(:going)
				@before_balance = @user.credit
				patch :pay, occurrence_id: @occ, format: :js
			end

			include_examples "unchanged occurrence", :going
		end

		context "User is going" do
			before do
				participate(:going)
				@before_balance = @user.credit
				patch :pay, occurrence_id: @occ, format: :js
			end

			it "should change user state to :prepaid" do
				expect(@occ.reload.participations.first.status).to eq(:prepaid)
			end

			it "should subtract the price from user's balance" do
				expect(@user.reload.credit).to eq(@before_balance - @event.price_in_credit)
			end

			it "should render subscription template" do
				expect(response).to render_template("subscription")
			end
		end

		context "User has not enough credit" do
			before do
				@user.credit = 8
				@user.save
				participate(:going)
				@before_balance = @user.credit
				patch :pay, occurrence_id: @occ, format: :js
			end

			include_examples "unchanged occurrence", :going
		end

		context "User has already paid" do
			before do
				participate(:prepaid)
				@before_balance = @user.credit
				patch :pay, occurrence_id: @occ, format: :js
			end
      login_partner

			include_examples "unchanged occurrence", :prepaid
		end
	end

	RSpec.shared_examples "simple getter" do |method|
		before(:each) do
			@event.partner = @partner
			@event.save
		end

		context "Partner isn't signed in" do
			before do
				sign_out :partner
			end

			it "redirects to root_path" do
				get method, occurrence_id: @occ
				expect(response).to redirect_to root_path
			end
		end

		context "Partner signed in" do
			it "renders checkin" do
				get method, occurrence_id: @occ
				expect(response).to render_template method.to_s
			end
		end
	end

	describe "GET #checkin" do
		login_partner

		include_examples "simple getter", :checkin
	end

	describe "GET #checkin_list" do
    login_partner

		include_examples "simple getter", :checkin_list
	end

	describe "PATCH #confirm_single_user" do
		login_partner

		before(:each) do
			@event.partner = @partner
			@event.save
		end

		describe "selects the right user" do
      context "User not existing" do
        before(:each) do
          patch :confirm_single_user, occurrence_id: @occ, username: "doublekill", format: :js
        end

        it "doesn't modfy occurrence" do
          expect(@occ).to eq(@occ.reload)
        end

        it "doesn't create new participation" do
          expect(@occ.participations.length).to eq(0)
        end
      end

      context "User does not participate" do
        before(:each) do
          FactoryGirl.create(:user, username: "chris")
          patch :confirm_single_user, occurrence_id: @occ, username: "chris", format: :js
        end

        it "adds a new participation" do
          expect(@occ.reload.participations.length).to eq(1)
        end

        it "adds the right user" do
          expect(@occ.participations.first.user.username).to eq("chris")
        end

        it "sets status to :paid_at_spot" do
          expect(@occ.participations.first.status).to eq(:paid_at_spot)
        end

        it "sets confirmation status to :new" do
          expect(@occ.participations.first.confirmed).to eq(:new)
        end
      end
    end

    describe "confirms user and sets the correct status" do
      before(:each) do
        @user = FactoryGirl.create(:participation)
        @occ.participations << @user
      end

      context "User pays at spot by cash" do
        before(:each) do
          patch :confirm_single_user, occurrence_id: @occ, participation_id: @user, format: :js
        end

        it "confirms the user" do
          expect(@user.reload.confirmed).to eq(:yes)
        end

        it "sets the status to :paid_at_spot" do
          expect(@user.reload.status).to eq(:paid_at_spot)
        end
      end

      context "User pays at spot with credit" do
        before(:each) do
          @before_credit = @user.user.credit
          patch :confirm_single_user, occurrence_id: @occ, participation_id: @user, pay_with_credit: true, format: :js
        end

        it "confirms the user" do
          expect(@user.reload.confirmed).to eq(:yes)
        end

        it "sets status to :paid_at_spot_with_credit" do
          expect(@user.reload.status).to eq(:paid_at_spot_with_credit)
        end

        it "subtracts the event's price from User's credit" do
          expect(@user.reload.user.credit).to eq(@before_credit - @event.price_in_credit)
        end
      end

      context "User has not enough credit" do
        before(:each) do
          u = @user.user
          u.credit = 5
          u.save
          patch :confirm_single_user, occurrence_id: @occ, participation_id: @user, pay_with_credit: true, format: :js
        end

        it "doesn't confirm user" do
          expect(@user.reload.confirmed).to eq(:no)
        end

        it "doesn't change status" do
          expect(@user.status).to eq(:going)
        end

        it "doesn't change User's credit" do
          expect(@user.user.credit).to eq(5)
        end
      end

      context "User is already confirmed" do
        before(:each) do
          @user.confirmed = :yes
          @user.status = :paid_at_spot
          @user.save
          @before_credit = @user.user.credit
          patch :confirm_single_user, occurrence_id: @occ, participation_id: @user, pay_with_credit: true, format: :js
        end

        it "doesn't change confirmation status" do
          expect(@user.reload.confirmed).to eq(:yes)
        end

        it "doesn't change participation status" do
          expect(@user.status).to eq(:paid_at_spot)
        end

        it "doesn't change User's credit" do
          expect(@user.user.credit).to eq(@before_credit)
        end
      end
    end
  end

  describe "PATCH #confirm_list" do
    login_partner

    before(:each) do
      @event.partner = @partner
      @event.save
    end

    it "confirms the selected participating users" do
      participation_list = [
          FactoryGirl.create(:participation, status: :going),
          FactoryGirl.create(:participation, status: :paid_at_spot_with_credit),
          FactoryGirl.create(:participation, status: :paid_at_spot),
          FactoryGirl.create(:participation, status: :going),
          FactoryGirl.create(:participation, confirmed: :yes, status: :going),
      ]
      @occ.participations << participation_list
      unconfirmed = FactoryGirl.create(:participation, confirmed: :no)
      patch :confirm_list, occurrence_id: @occ, participations: participation_list.map{|p| p._id}, format: :js
      participation_list.map{ |p|
        expect(p.reload.confirmed).to eq(:yes)
      }
      expect(unconfirmed.reload.confirmed).to eq(:no)
      [0,3,4].map{ |i|
        expect(participation_list[i].status).to eq(:paid_at_spot)
      }
    end
  end

  describe "PATCH #unconfirm" do
    login_partner

    before(:each) do
      @event.partner = @partner
      @event.save
    end

    context "status is prepaid" do
      before(:each) do
        p = FactoryGirl.create(:participation, status: :prepaid, confirmed: :yes)
        @occ.participations << p
        patch :unconfirm, occurrence_id: @occ, participation_id: p, format: :js
      end

      it "doesn't change status" do
        expect(@occ.reload.participations.first.status).to eq(:prepaid)
      end

      it "unconfirms the user" do
        expect(@occ.reload.participations.first.confirmed).to eq(:no)
      end
    end

    context "status is paid_at_spot" do
      before(:each) do
        p = FactoryGirl.create(:participation, status: :paid_at_spot, confirmed: :yes)
        @occ.participations << p
        patch :unconfirm, occurrence_id: @occ, participation_id: p, format: :js
      end

      it "changes status to going" do
        expect(@occ.reload.participations.first.status).to eq(:going)
      end

      it "unconfirms the user" do
        expect(@occ.reload.participations.first.confirmed).to eq(:no)
      end
    end

    context "status is paid_at_spot_with_credit" do
      before(:each) do
        p = FactoryGirl.create(:participation, status: :paid_at_spot_with_credit, confirmed: :yes)
        @occ.participations << p
        @before_credit = p.user.credit
        patch :unconfirm, occurrence_id: @occ, participation_id: p, format: :js
      end

      it "changes status to going" do
        expect(@occ.reload.participations.first.status).to eq(:going)
      end

      it "unconfirms the user" do
        expect(@occ.reload.participations.first.confirmed).to eq(:no)
      end

      it "pays back the event's price" do
        expect(@occ.reload.participations.first.user.credit).to eq(@before_credit + @event.price_in_credit)
      end
    end

    context "User has been (accidentally) added during registration" do
      before(:each) do
        p = FactoryGirl.create(:participation, status: :paid_at_spot_with_credit, confirmed: :new)
        @user = p.user
        @occ.participations << p
        @before_credit = @user.credit
        patch :unconfirm, occurrence_id: @occ, participation_id: p, format: :js
      end

      it "removes user from participation list" do
        expect(@occ.reload.participations.length).to eq(0)
      end

      it "pays back the event price to the user" do
        expect(@user.reload.credit).to eq(@before_credit + @event.price_in_credit)
      end
    end
  end

  describe "PATCH #finalize" do
    login_partner

    before(:each) do
      @event.partner = @partner
      @event.save
      @prepaid_confirmed = FactoryGirl.create(:participation, confirmed: :yes, status: :prepaid)
      @going_unconfirmed = FactoryGirl.create(:participation, confirmed: :no, status: :going)
      @prepaid_unconfirmed = FactoryGirl.create(:participation, confirmed: :no, status: :prepaid)
      @before_credit = @prepaid_unconfirmed.user.credit
      @occ.participations << [@prepaid_confirmed, @going_unconfirmed, @prepaid_unconfirmed]
      patch :finalize, occurrence_id: @occ
    end

    it "finalizes the occurrance" do
      expect(@occ.reload.finalized).to eq(true)
    end

    it "adds event to user's visited events" do
      expect(@prepaid_confirmed.reload.user.visited_events).to include(@event)
    end

    it "sets the status to not_attended for unconfirmed" do
      expect(@going_unconfirmed.reload.status).to eq(:not_attended)
    end

    it "sets the status to not_attended for unconfirmed paid user" do
      expect(@prepaid_unconfirmed.reload.status).to eq(:not_attended)
    end

    it "pays back half the price for unconfirmed paid user" do
      expect(@prepaid_unconfirmed.reload.user.credit).to eq(@before_credit + 0.5 * @event.price_in_credit)
    end

    it "redirects to event page" do
      expect(response).to redirect_to(event_path(@event))
    end
  end
end