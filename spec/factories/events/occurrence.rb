require 'faker'

FactoryGirl.define do
	factory :occurrence, class: "Events::Occurrence" do
		transient do
			going_count 0
			paid_count 0
		end

		date{ Faker::Time.forward(10) }

		after :create do |occ,evaluator|
			create_list(:participation, evaluator.going_count, occurrence: occ)
			create_list(:participation_paid, evaluator.paid_count, occurrence: occ)
		end
	end
end