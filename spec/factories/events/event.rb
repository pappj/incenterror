require 'faker'

FactoryGirl.define do
	factory :events, class: Event do
		title{ Faker::Book.title }
		description{ Faker::Hipster.paragraph(2) }
		price 1000
		partner

		transient do
			comment_count 0
		end

		after :create do |event,evaluator|
			create_list(:comment, evaluator.comment_count)
		end

		factory :presentation, class: "Events::Presentation" do
			transient do
				occ_count 0
			end

			after :create do |event,evaluator|
				create_list(:occurrence, evaluator.occ_count, presentation: event)
			end
		end
	end
end