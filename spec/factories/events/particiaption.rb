FactoryGirl.define do
	factory :participation, class: "Events::Participation" do
		status :going
		user

		factory :participation_paid do
			status :prepaid
		end
	end
end