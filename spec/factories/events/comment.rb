require 'faker'

FactoryGirl.define do
	factory :comment do
		text{ Faker::Hipster.paragraph }
		posted{ Faker::Time.backward(15) }
		association :commenter, factory: :user
	end
end