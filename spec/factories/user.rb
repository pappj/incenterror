require 'faker'

FactoryGirl.define do
	factory :user do
		email { Faker::Internet.email }
		password '111111'
		username { Faker::Internet.user_name }
	end
end