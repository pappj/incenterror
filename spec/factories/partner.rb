require 'faker'

FactoryGirl.define do
	factory :partner do
		email { Faker::Internet.email }
		password '111111'
		name { Faker::Name.name }
	end
end