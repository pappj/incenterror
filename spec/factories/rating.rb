FactoryGirl.define do
  factory :rating do
    value{ rand(5)+1 }
    ratable FactoryGirl.create(:events)
    user
  end
end