class User::LevelHandler

  def initialize level, knowledge, self_knowledge
    @idx = level-1
    @knowledge = knowledge
    @self_knowledge = self_knowledge
  end

  def knowledge_status
    "#{@knowledge}/#{KNOWLEDGE_THRESHOLDS[@idx]}"
  end

  def self_knowledge_status
    "#{@self_knowledge}/#{SELF_KNOWLEDGE_THRESHOLDS[@idx]}"
  end

  def xp_status
    kdg = [@knowledge, KNOWLEDGE_THRESHOLDS[@idx]].min
    s_kdg = [@self_knowledge, SELF_KNOWLEDGE_THRESHOLDS[@idx]].min
    xp = kdg + s_kdg
    next_level_xp = KNOWLEDGE_THRESHOLDS[@idx] + SELF_KNOWLEDGE_THRESHOLDS[@idx]
    "#{xp}/#{next_level_xp}"
  end

  def knowledge_pct
    "#{kdg_pct.round(2)}%"
  end

  def self_knowledge_pct
    "#{s_kdg_pct.round(2)}%"
  end

  def xp_pct
    "#{((kdg_pct + s_kdg_pct)/2).round(2)}%"
  end

  private
    KNOWLEDGE_THRESHOLDS = [200, 500, 900, 1400, 2000, 2700, 3500]
    SELF_KNOWLEDGE_THRESHOLDS = [350, 650, 1050, 1550, 2150, 2850, 3650]

    def max_level
      KNOWLEDGE_THRESHOLDS.length
    end

    def kdg_pct
      [@knowledge.to_f/KNOWLEDGE_THRESHOLDS[@idx].to_f*100.0, 100.0].min
    end

    def s_kdg_pct
      [@self_knowledge.to_f/SELF_KNOWLEDGE_THRESHOLDS[@idx].to_f*100, 100].min
    end
end