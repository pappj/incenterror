class Admin::UserHandler
  def initialize(user)
    @user = user
  end

  def adjust_credit(amount)
    @user.credit += amount.to_i
    @user.save

    @user.credit
  end
end