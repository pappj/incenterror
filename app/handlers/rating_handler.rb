class RatingHandler
  def initialize(ratings_array)
    @all_ratings = ratings_array
  end

  def calc_rating_aggregate(session = nil)
    rating_aggregate = calc_rating_aggregate_impl

    if(session)
      session[:rating_aggregate_tvalue] = rating_aggregate.total_value
      session[:rating_aggregate_count] = rating_aggregate.count
    end

    rating_aggregate
  end

  def load_from_session_and_calc_rating_aggregate(rating_value, session)
    rating_aggregate = if(session.key?(:rating_aggregate_tvalue) && session.key?(:rating_aggregate_count))
      total_value = session[:rating_aggregate_tvalue]
      count = session[:rating_aggregate_count]
      RatingAggregate.new(total_value, count)
    else
      calc_rating_aggregate_impl
    end

    rating_aggregate.adjust(rating_value)

    rating_aggregate
  end

  private

  def calc_rating_aggregate_impl
    total_value = @all_ratings.inject(0){|sum,x| sum += x.value}
    count = @all_ratings.length

    RatingAggregate.new(total_value, count)
  end


end