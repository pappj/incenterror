class Events::Occurrences::ConfirmationHandler
  def initialize(occurrence)
    @occ = occurrence
  end

  def confirm_single_user(participation_id, username, pay_with_credit)
    participation = get_participation(participation_id, username)
    if participation.nil?
      changed = :no
    else
      if participation.new_record?
        confirm_status = :new
        changed = :added
      else
        confirm_status = :yes
        changed = :moved
      end

      update_participation(participation, confirm_status, pay_with_credit)
    end

    Events::OccurrencePresenter.new(@occ, changed, "", participation)
  rescue Exception => e
    Events::OccurrencePresenter.new(@occ, :no, e.message)
  end

  def confirm_list(participation_ids)
    participations = Events::Participation.where({:_id.in => participation_ids}).to_a
    participations.each do |p|
      p.confirmed = :yes
      if p.status == :going
        p.status = :paid_at_spot
      end
      p.save
    end

    Events::OccurrencePresenter.new(@occ, :no, "", participations)
  end

  def unconfirm_participation(participation_id)
    changed = :no
    participation = @occ.participations.find(participation_id)
    unless participation.nil? || participation.confirmed == :no
      if participation.status == :paid_at_spot_with_credit
        Events::Payment.new(participation.user, @occ.event.price_in_credit).pay_back
        participation.user.save
      end

      if participation.confirmed == :new
        participation.destroy
      else
        participation.confirmed = :no
        unless participation.status == :prepaid
          participation.status = :going
        end
        participation.save

        changed = :undo
      end
    end

    Events::OccurrencePresenter.new(@occ, changed, "", participation)
  end

  def finalize
    @occ.finalized = true
    @occ.save

    @occ.participations.each do |p|
      if p.confirmed?
        p.user.attended(@occ.event)
      else
        if p.status == :prepaid
          Events::Payment.new(p.user, @occ.event.price_in_credit*0.5).pay_back
        end

        p.status = :not_attended
        p.save
      end
    end
  end

  private
    def get_participation(participation_id, username)
      if participation_id.nil?
        user = User.find_by({username: username})
        if user.nil?
          raise I18n.t('messages.errors.no_user')
        end

        participation = @occ.participations.where({user_id: user}).first
        if participation.nil?
          participation = Events::Participation.new(status: :going, confirmed: false, user: user, occurrence: @occ)
        end
      else
        participation = @occ.participations.where({_id: participation_id}).first
      end

      participation
    end

    def update_participation(participation, confirm_status, pay_with_credit)
      if participation.confirmed?
        raise I18n.t('messages.errors.user_already_confirmed')
      end

      if participation.status == :going
        if pay_with_credit
          Events::Payment.new(participation.user, @occ.event.price_in_credit).pay
          participation.status = :paid_at_spot_with_credit
        else
          participation.status = :paid_at_spot
        end
      end

      participation.confirmed = confirm_status
      participation.save
    end
end