class Events::Occurrences::SubscriptionHandler
  attr_reader :occ
  attr_reader :user

  def initialize(occurrence, user)
    @occ = occurrence
    @user = user
  end

  def subscribe
    participation = @occ.participations.where(user: @user).first
    if participation.nil?
      participation = create_participation
      changed = :added
    else
      unsubscribe(participation)
      participation = nil
      changed = :removed
    end
    Events::OccurrencePresenter.new(@occ, changed, "", participation, @user)
  rescue PsyCraftErrors::DatabaseError
    Events::OccurrencePresenter.new(@occ, :no, I18n.t("messages.errors.database_error"))
  end

  def pay
    changed = :no
    error = ""
    participation = @occ.participations.where(user: @user).first
    unless participation.nil?
      if participation.status == :going
        begin
          payment = Events::Payment.new(@user, @occ.event.price_in_credit)
          payment.pay
          participation.status = :prepaid
          unless participation.save
            raise PsyCraftErrors::DatabaseError
          end
          changed = :paid
        rescue PsyCraftErrors::NotEnoughCredit
          error = I18n.t('messages.errors.not_enough_credit')
        rescue PsyCraftErrors::DatabaseError
          error = I18n.t('messages.errors.database_error')
        end
      end
    end

    Events::OccurrencePresenter.new(@occ, changed, error, participation, @user)
  end

  private
    def create_participation
      participation = Events::Participation.new(status: :going, user: @user, occurrence: @occ)
      @occ.participations << participation
      participation
    end

    def unsubscribe(participation)
      if participation.status == :prepaid
        price_factor = @occ.date.to_time - 60*60 > Time.now ? 1 : 0.5
        payment = ::Events::Payment.new(@user, @occ.event.price_in_credit * price_factor)
        payment.pay_back
      end
      participation.destroy
    end
end