module EventsHelper
  def edit_event_path evt
    case evt.class
      when Events::Presentation
        edit_events_presentation_path(evt)
      when Events::Course
        edit_events_course_path(evt)
      when Events::Courses::Presentation
        edit_events_courses_presentation_path(evt)
    end
  end
end
