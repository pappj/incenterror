module RatingsHelper
  def can_review? ratable_object
    return false unless user_signed_in?
    rating = Rating.where({ratable: ratable_object, user: current_user}).first
    rating.nil?
  end

  def get_rating_aggregate_for object
    rh = RatingHandler.new(object.ratings)
    rh.calc_rating_aggregate(session)
  end
end