module UsersHelper
  def get_user_nav_link label, user, subpage
    active = ""
    active = " class=\"active\"" if(label == subpage)
    link = "<li#{active}>"
    link += link_to(I18n.t("helpers.label.user.nav.#{label}"), public_send("user_#{label}_path", user),
                     class: "nav-link")
    link += "</li>"
    link.html_safe
  end
end
