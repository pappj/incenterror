module ApplicationHelper
	def authorized?
		user_signed_in? || partner_signed_in?
	end

	def me? person
		if user_signed_in?
			person == current_user
		elsif partner_signed_in?
			person == current_partner
    elsif admin_signed_in?
      true
		else
			false
		end
	end

	def user_eligible_for_quest? user, quest
		user.assigned_quests.exclude?(quest) && user.completed_quests.exclude?(quest) && (quest.dependencies - user.completed_quests).empty?
	end

	def get_quest_assign_link quest
		link = ""
		if user_signed_in?
			if current_user.completed_quests.include?(quest)
				link = "(#{I18n.t('labels.quests.completed')})"
			elsif current_user.assigned_quests.include?(quest)
				link =  link_to(I18n.t('labels.quests.decline'), quest_assignment_path(quest), {method: :patch, remote: true})
			else
				if user_eligible_for_quest?(current_user, quest)
					link = link_to(I18n.t('labels.quests.take_it'), quest_assignment_path(quest), {method: :patch, remote: true})
				else
					link = "(#{I18n.t('labels.quests.locked')})"
				end
			end
		end

		link
	end

  def get_quest_visibility user
    user.quest_public ? I18n.t('labels.visibility.public') : I18n.t('labels.visibility.private')
  end

	def get_quest_assign_status user, quest
		status = ""
		if user.completed_quests.include?(quest)
			status = "(#{I18n.t('labels.quests.completed')})"
		elsif user.assigned_quests.include?(quest)
			status = "(#{I18n.t('labels.quests.active')})"
		else
			if user_eligible_for_quest?(user, quest)
				status = "(#{I18n.t('labels.quests.available')})"
			else
				status = "(#{I18n.t('labels.quests.locked')})"
			end
		end

		status
	end

	def get_event_subscription_link occ
		link = "<div id='sublink_#{occ._id}' class='flex-container button-container'>"
		if user_signed_in?
      html_params = {method: :patch, remote: true, class: "pc-button pc-teal"}
			if occ.date > Time.now
				participation = occ.participations.where(user: current_user).first
				if participation.nil?
					link << link_to(I18n.t('labels.occurrence.im_going'), subscription_path(occ), html_params)
				elsif participation.status == :going || participation.status == :prepaid
					link << link_to(I18n.t('labels.occurrence.not_going'), subscription_path(occ), html_params)
					if participation.status == :going
						link << link_to(I18n.t('labels.occurrence.pay_it'), pay_occurrence_path(occ), html_params)
					end
				end
			end
		end

		link << "</div>"
		link.html_safe
	end
end