class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  require 'psy_craft_errors/error'

  devise_group :anyone, contains: [:user, :partner, :admin]
  devise_group :client, contains: [:user, :partner]
end
