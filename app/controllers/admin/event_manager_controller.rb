class Admin::EventManagerController < ApplicationController
  before_action :verify_admin

  def show
    render("admin/event_manager/main")
  end

  def verify_admin
    unless admin_signed_in?
      redirect_to root_path
    end
  end
end