class Admin::UserManagerController < ApplicationController
  before_action :verify_admin

  def show
    render("admin/user_manager/main")
  end

  def get_user
    user = User.find_by({username: params[:username]})

    respond_to do |format|
      format.js{render("admin/user_manager/get_user.js.erb", locals: {user: user, name: params[:username]})}
    end
  end

  def adjust_credit
    user = User.find(params[:credit_user_id])
    new_credit = Admin::UserHandler.new(user).adjust_credit(params[:amount])

    respond_to do |format|
      format.js{ render("admin/user_manager/adjust_credit", locals: {credit: new_credit}) }
    end
  end

  private
    def verify_admin
      unless admin_signed_in?
        redirect_to root_path
      end
    end
end