class Admin::PartnerManagerController < ApplicationController
  include RatingsHelper
  before_action :verify_admin

  def show
    if(params.key?(:id))
      begin
        partner = Partner.find(params[:id])
        rating = get_rating_aggregate_for(partner)
        session[:selected_partner_id] = partner._id.to_s #overwrite the currently saved partner
        events = partner.events
      rescue
        flash[:err] = "The given partner id cannot be found"
      end
    else
      events = []
    end

    all_partners = Partner.all.to_a.sort_by!(&:name)

    render("admin/partner_manager/main", locals: {partners: all_partners, selected_partner: partner,
                                                  rating: rating, events: events})
  end

  def select_partner
    partner = Partner.where({"$or" => [{email: params[:id]}, {name: params[:id]}]}).first

    if partner
      session[:selected_partner_id] = partner._id
      flash[:notice] = "Partner #{partner.name} is selected."
      redirect_to partner_manager_path(partner)
    else
      flash[:err] = "There is no such partner."
      redirect_to partner_manager_path
    end
  end

  def deselect_partner
    session.delete(:selected_partner_id)
    redirect_to partner_manager_path
  end

  def verify_admin
    unless admin_signed_in?
      redirect_to root_path
    end
  end
end