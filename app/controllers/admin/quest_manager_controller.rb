class Admin::QuestManagerController < ApplicationController
  before_action :verify_admin

  def show
    render("admin/quest_manager/main")
  end

  def verify_admin
    unless admin_signed_in?
      redirect_to root_path
    end
  end
end