class Admin::SessionsController < Devise::SessionsController
  def create
    super
    sign_out :user
    sign_out :partner
  end
end