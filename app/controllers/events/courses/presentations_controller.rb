class Events::Courses::PresentationsController < ApplicationController
  include ApplicationHelper

  def show
    event = Event.find(params[:id])
    rh = RatingHandler.new(event.ratings)
    rating_aggregate = rh.calc_rating_aggregate(session)
    render("events/show", locals: {presenter: Events::Courses::PresentationPresenter.new(event, RatingPresenter.new(rating_aggregate)),
                                   edit_path: edit_events_courses_presentation_path(event),
                                   comment: Comment.new})
  end

  def edit
    if partner_signed_in?
      session[:redirect_to] = request.referer
      event = Event.find(params[:id])
      if event.partner == current_partner
        render("events/edit", locals: {presenter: Events::PresentationPresenter.new(event),
                                       main_path: events_courses_presentation_path(event)})
      else
        redirect_to request.referer.to_s
      end
    else
      redirect_to request.referer.to_s
    end
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  def update
    event = Event.find(params[:id])
    if me? event.partner
      param_hash = pres_params
      param_hash[:occurrences_attributes] = param_hash.delete(:future_occurrences_attributes) || []
      event.update_attributes(param_hash)
    end

    redirect_to session.delete(:redirect_to)
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  protected
  def pres_params
    params.require(:events_courses_presentation).permit(:partner, :title, :description, :price, future_occurrences_attributes: [:id, :date, :_destroy])
  end

  def valid?(occurrences)
    valid = true
    occurrences.each do |occ|
      valid = valid && occ.second[:date] > Time.now
    end

    return valid
  end
end
