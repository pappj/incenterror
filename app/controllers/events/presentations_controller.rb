class Events::PresentationsController < ApplicationController
  include ApplicationHelper

  def show
    event = Event.find(params[:id])
    rh = RatingHandler.new(event.ratings)
    rating_aggregate = rh.calc_rating_aggregate(session)
    render("events/show", locals: {presenter: Events::PresentationPresenter.new(event, RatingPresenter.new(rating_aggregate)),
                                   edit_path: edit_events_presentation_path(event),
                                   comment: Comment.new})
  end

  def new
    if partner_signed_in? || admin_signed_in?
      event = Events::Presentation.new
      event.occurrences.new
    else
      redirect_to root_path
    end

    render("events/new", locals: {presenter: Events::PresentationPresenter.new(event),
                                  partner_id: partner_id})
  end

  def edit
    if partner_signed_in? || admin_signed_in?
      session[:redirect_to] = request.referer
      event = Event.find(params[:id])
      if event.partner == current_partner || admin_signed_in?
        render("events/edit", locals: {presenter: Events::PresentationPresenter.new(event),
                                       main_path: events_presentation_path(event),
                                       partner_id: partner_id})
      else
        redirect_to request.referer.to_s
      end
    else
      redirect_to request.referer.to_s
    end
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  def create
    if partner_signed_in? || admin_signed_in?
      param_hash = pres_params
      param_hash[:occurrences_attributes] = param_hash.delete(:future_occurrences_attributes) || []
      evt = Events::Presentation.new(param_hash)
      evt.save
      redirect_to partner_path(param_hash[:partner])
    else
      redirect_to root_path
    end
  end

  def update
    event = Event.find(params[:id])
    if me? event.partner || admin_signed_in?
      param_hash = pres_params
      param_hash[:occurrences_attributes] = param_hash.delete(:future_occurrences_attributes) || []
      event.update_attributes(param_hash)
    end

    redirect_to session.delete(:redirect_to)
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  protected
    def pres_params
      params.require(:events_presentation).permit(:partner, :title, :description, :price, future_occurrences_attributes: [:id, :date, :_destroy])
    end

    def partner_id
      partner_signed_in? ? current_partner._id.to_s : session[:selected_partner_id]
    end

    def valid?(occurrences)
      valid = true
      occurrences.each do |occ|
        valid = valid && occ.second[:date] > Time.now
      end

      return valid
    end
end
