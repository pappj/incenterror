class Events::OccurrencesController < ApplicationController
	include ApplicationHelper
	before_action :get_occurrence
	before_action :verify_partner, only: [:checkin, :checkin_list, :confirm_single_user, :confirm_list, :unconfirm, :finalize]

	def subscription
		presenter = Events::OccurrencePresenter.new(@occ)
		if user_signed_in?
			presenter = Events::Occurrences::SubscriptionHandler.new(@occ, current_user).subscribe
    end

		respond_to do |format|
			format.js{ render "subscription", locals: {presenter: presenter}}
		end
	end

	def pay
    presenter = Events::OccurrencePresenter.new(@occ)
		if user_signed_in?
      presenter = Events::Occurrences::SubscriptionHandler.new(@occ, current_user).pay
		end

		respond_to do |format|
      format.js{ render "subscription", locals: {presenter: presenter}}
		end
	end

	def checkin
		non_confirmed = @occ.participations.where(confirmed: :no)
		@paid_users = non_confirmed.where(status: :prepaid).sort_by{|u| u.username.downcase}
		@going_users = non_confirmed.where(status: :going).sort_by{|u| u.username.downcase}
		@confirmed_users = @occ.participations.any_of({confirmed: :yes},{confirmed: :new}).sort_by{|u| u.username.downcase}
	end

	def checkin_list
		@pending_users = @occ.participations.where(confirmed: :no).sort_by{|u| u.username.downcase}
		@confirmed_users = @occ.participations.any_of({confirmed: :yes},{confirmed: :new}).sort_by{|u| u.username.downcase}
	end

	def confirm_single_user
		ch = Events::Occurrences::ConfirmationHandler.new(@occ)
    presenter = ch.confirm_single_user(params[:participation_id], params[:username], params[:pay_with_credit])

		respond_to do |format|
			format.js{ render "confirm_single_user", locals: {presenter: presenter}}
		end
	end

	def confirm_list
    ch = Events::Occurrences::ConfirmationHandler.new(@occ)
    presenter = ch.confirm_list(params[:participations])

		respond_to do |format|
			format.js{ render "confirm_list", locals: {presenter: presenter}}
		end
	end

	def unconfirm
    ch = Events::Occurrences::ConfirmationHandler.new(@occ)
    presenter = ch.unconfirm_participation(params[:participation_id])

    respond_to do |format|
			format.js{ render "unconfirm", locals: {presenter: presenter}}
		end
	end

	def finalize
    Events::Occurrences::ConfirmationHandler.new(@occ).finalize

		redirect_to event_path(@occ.event)
	end

  private
    def get_occurrence
      @occ = ::Events::Occurrence.find(params[:occurrence_id])
      if @occ.nil?
        @error = I18n.t('messages.errors.no_occurrence')
        redirect_to root_path
      end
    end

    def verify_partner
      if !me?(@occ.partner)
        @error = I18n.t('messages.errors.permission_denied')
        redirect_to root_path
      end
    end
end