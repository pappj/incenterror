class RatingsController < ApplicationController
  def create
    if(user_signed_in?)
      id, type = params[:rating][:ratable_id], params[:rating][:ratable_type]

      ratable = Object.const_get(type.capitalize).find(id)

      rh = RatingHandler.new(ratable.ratings)
      rating_aggregate = rh.load_from_session_and_calc_rating_aggregate(params[:rating][:value].to_i, session)

      rating = Rating.new(rating_params.merge({ratable: ratable, user: current_user}))
      rating.save

      respond_to do |format|
        format.js{ render("ratings/add", locals: {new_rating: rating, rating_aggr: rating_aggregate}) }
      end
    else
      redirect_to root_path
    end
  end

  private

  def rating_params
    params.require(:rating).permit(:value, :review)
  end

end