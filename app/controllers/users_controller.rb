class UsersController < ApplicationController
	include ApplicationHelper
  def show
  	usr = User.find(params[:id])
    sp = params[:sp] || "personal"
    level = User::LevelHandler.new(usr.level, usr.knowledge, usr.self_knowledge)

    render("users/show", locals: {user: usr, subpage: sp, level_data: level})
  end

  def edit
  end

  def quest_visibility
  	@user = User.find(params[:id])
    @changed = :no
  	if me? @user
  		@user.quest_public = !@user.quest_public
  		@user.save
      @changed = :yes
  	end

    respond_to do |format|
      format.js
    end
  end
end
