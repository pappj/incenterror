class HomeController < ApplicationController
  def index
    render('home/index', locals: {disable_main_container: true})
  end

  def logout
    sign_out_all_scopes

    redirect_to root_path
  end
end
