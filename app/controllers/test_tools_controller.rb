class TestToolsController < ApplicationController
	def new_user
		case params[:type]
		when "user"
			@user = User.new(username: params[:username], email: params[:username].gsub(" ", "") + "@gmail.com",
											 password: "111111", password_confirmation: "111111")
			@user.save
		when "partner"
			@user = Partner.new(name: params[:username], email: params[:username].gsub(" ", "") + "@gmail.com",
													password: "111111", password_confirmation: "111111")
			@user.save
		end

		sign_out_all_scopes
		sign_in @user

		redirect_to request.referer.to_s
	end

	def change_user
		@user = User.where({username: params[:username]}).first
		if @user.nil?
			@user = Partner.where({name: params[:username]}).first
		end	

		unless @user.nil?
			sign_out_all_scopes
			sign_in @user
		end

		redirect_to request.referer.to_s
	end
end