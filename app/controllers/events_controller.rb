class EventsController < ApplicationController
  include ApplicationHelper

	def index
    presenters = Event.all.map{ |e| Object.const_get("#{e._type}Presenter").new(e, "")}
    render("index", locals: {presenters: presenters})
	end

	def new
		if partner_signed_in? || admin_signed_in?
			case params[:event_type]
			when "presentations"
				redirect_to new_events_presentation_path
      when "courses"
        redirect_to new_events_course_path
			else
				redirect_to root_path
			end
		else
			redirect_to root_path
		end
	end

	def destroy
		if partner_signed_in? || admin_signed_in?
			event = Event.find(params[:id])
			if event && (admin_signed_in? || me?(event.partner))
				event.destroy
			end
		end

		redirect_to request.referer.to_s
	rescue ActionController::RedirectBackError
	  redirect_to root_path
	end

	def create_comment
		@changed = :no
		if authorized?
			@comment = Comment.new(comment_params)
			@comment.event = params[:event_id]
			@comment.posted = Time.now
			if user_signed_in?
				@comment.commenter = current_user
			elsif partner_signed_in?
				@comment.commenter = current_partner
			end
			@comment.save
			@event_id = params[:event_id]
			@changed = :added
		end

		respond_to do |format|
			format.js{render("comments/create_comment")}
		end
	end

	def destroy_comment
		@changed = :no
		if authorized?
			@comment = Comment.find(params[:comment_id])
			if me? @comment.commenter
				@comment_id = @comment._id
				@comment.destroy
				@changed = :deleted
			end
		end

		respond_to do |format|
			format.js{render("comments/destroy_comment")}
		end
  end

  def comment_params
    params.require(:comment).permit(:text)
  end
end
