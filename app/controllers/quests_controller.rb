class QuestsController < ApplicationController
	include ApplicationHelper

  before_action :verify_admin, only: [:new, :create, :edit, :update, :destroy]

	def index
		@quests = ::Quests::Quest.all
	end

	def show
		@quest = ::Quests::Quest.find(params[:id])
	end

	def new
		render("quests/new", locals: {quest: Quests::GenericQuest.new, all_quests: all_quests})
	end

	def edit
		quest = ::Quests::Quest.find(params[:id])

		render("quests/edit", locals: {quest: quest, all_quests: all_quests})
	end

	def create
		par = quest_params
		quest = ::Quests::GenericQuest.new(par)
		quest.save!

    respond_to do |format|
      format.js{ head :ok }
    end
	end

	def update
		par = quest_params
		quest = ::Quests::Quest.find(params[:id])
		quest.dependencies.clear unless par.include?(:dependency_ids)
		quest.update_attributes(par)


		respond_to do |format|
			format.js{ head :ok }
		end
	end

	def destroy
		@quest = ::Quests::Quest.find(params[:id])
		@quest.destroy

		redirect_to quests_path
	end

	def assignment
		@changed = :no
		if user_signed_in?
			@quest = ::Quests::Quest.find(params[:id])
			if current_user.assigned_quests.include? @quest
				current_user.assigned_quests.delete(@quest)
				@changed = :declined
			elsif user_eligible_for_quest? current_user, @quest
				current_user.assigned_quests << @quest
				@changed = :assigned
			end
		end

		respond_to do |format|
			format.js
		end
	end

	protected
		def quest_params
			params.require(:quest).permit(:title, :description, dependency_ids: [],
																		tasks_attributes: [:id, :description, :event_id, :_destroy])
		end

		def all_quests
			Quests::Quest.all.map{|q| {id: q._id.to_s, title: q.title}}
		end

    def verify_admin
      unless admin_signed_in?
        redirect_to root_path
      end
    end
end
