class PartnerControllers::SessionsController < Devise::SessionsController
# before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    redirect_to root_path
  end

  # POST /resource/sign_in
  def create
    self.resource = warden.authenticate(auth_options)
    sign_in(resource_name, resource) if resource
    if resource
      flash[:notice] = I18n.t("devise.sessions.signed_in")
    else
      flash[:err] = resource ? "" : I18n.t("devise.failure.invalid")
    end

    redirect_to root_path
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
