class PartnerControllers::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
# before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    redirect_to root_path
  end

  # POST /resource
  def create
    partner_email_count = Partner.where({email: params[:partner][:email]}).count
    partner_name_count = Partner.where({name: params[:partner][:name]}).count
    partner_count = partner_email_count + partner_name_count
    if partner_count == 0
      super
    else
      err = ""
      err += "#{params[:partner][:name]} is already in use as name." if partner_name_count > 0
      err += "#{params[:partner][:email]} is already in use as email." if partner_email_count > 0
      flash[:err] = err
      redirect_to request.referer.to_s
    end
  end

  def sign_up(resource_name, resource)
    #Do nothing when a new partner is signed_up
  end

  def after_sign_up_path_for new_partner
    partner_manager_path(new_partner)
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
