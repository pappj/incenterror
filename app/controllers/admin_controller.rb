class AdminController < ApplicationController
  def show
    partner_id = if(session.key?(:selected_partner_id))
      session[:selected_partner_id]
    else
      ""
    end

    render("admin/show", locals: {partner_id: partner_id})
  end
  
  def update_password
    unless admin_signed_in?
      redirect_to root_path
    end

    p = password_params
    if p[:password] != p[:password_confirmation]
      flash[:err] = "Your new passwords don't match"
    elsif current_admin.update_with_password(password_params)
      bypass_sign_in(current_admin)
      flash[:notice] = "You changed your password successfully"
    else
      flash[:err] = "Your password is incorrect"
    end

    redirect_to hq_path
  end
  
  private
    def password_params
      params.require(:admin).permit(:current_password, :password, :password_confirmation)
    end
end