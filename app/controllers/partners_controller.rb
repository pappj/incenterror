class PartnersController < ApplicationController
  def show
  	partner = Partner.find(params[:id])
    is_owner = partner_signed_in? && partner == current_partner
    rh = RatingHandler.new(partner.ratings)
    rating_aggregate = rh.calc_rating_aggregate(session)
    presenter = PartnerPresenter.new(partner, "", is_owner, RatingPresenter.new(rating_aggregate))
    render("show", locals: {presenter: presenter})
  end

  def edit
  end
end
