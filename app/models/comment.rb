class Comment
	include Mongoid::Document
	include ApplicationHelper
	include Rails.application.routes.url_helpers

	field :text, type: String
	field :posted, type: DateTime

	belongs_to :commenter, polymorphic: true

	belongs_to :event
end