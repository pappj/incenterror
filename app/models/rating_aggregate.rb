class RatingAggregate
  attr_reader :count, :total_value

  def initialize(total_value, count)
    @total_value = total_value
    @count = count
  end

  def value
    average.round(2)
  end

  def percent
    average / 5.0 * 100.0
  end

  def adjust(new_value)
    @total_value += new_value
    @count += 1
  end

  private

  def average
    if @count == 0
      0.0
    else
      @total_value.to_f / @count.to_f
    end
  end

end