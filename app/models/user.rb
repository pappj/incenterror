class User
  include Mongoid::Document

  ### Devise fileds
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, authentication_keys: [:login_id]

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,  , type: String
  # field :confirmed_at,        , type: Time
  # field :confirmation_sent_at,, type: Time
  # field :unconfirmed_email,   , type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts,, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,   , type: String # Only if unlock strategy is :email or :both
  # field :locked_at,      , type: Time

  ### Custom fields
  field :username, type: String
  field :level, type: Integer, default: 1

  field :knowledge, type: Integer, default: 0
  field :self_knowledge, type: Integer, default: 0

  def xp
    knowledge + self_knowledge
  end

  field :credit, type: Integer, default: 25

  embeds_one :personality, autobuild: true
  embeds_one :psychology, autobuild: true
  embeds_one :skill, autobuild: true

  field :quest_public, type: Boolean, default: false

  has_many :comments, as: :commenter
  has_many :ratings
  has_many :completed_quests, class_name: "Quests::UserQuest"
  has_many :assigned_quests, class_name: "Quests::UserQuest"
  has_many :occurrences, class_name: "Events::Participation"
  has_and_belongs_to_many :visited_events, class_name: "Event"

  def attended event
    visited_events << event
  end

  def self.find_for_authentication(params)
    where("$or" => [{email: params[:login_id].downcase}, {username: params[:login_id]}]).first
  end
end

class Personality
  include Mongoid::Document

  field :charisma, type: Integer, default: 1
  field :thrustworthyness, type: Integer, default: 1
  field :communication, type: Integer, default: 1
  field :connectivity, type: Integer, default: 1
  field :household, type: Integer, default: 1

  embedded_in :user
end

class Psychology
  include Mongoid::Document

  field :extraversion, type: Integer, default: 1
  field :openness, type: Integer, default: 1
  field :friendlyness, type: Integer, default: 1
  field :exactitude, type: Integer, default: 1
  field :neuroticism, type: Integer, default: 1

  embedded_in :user
end

class Skill
  include Mongoid::Document

  field :resilience, type: Integer, default: 1
  field :anxiety, type: Integer, default: 1
  field :satisfaction, type: Integer, default: 1
  field :experiencepursuance, type: Integer, default: 1
  field :yschema, type: Integer, default: 1

  field :cooperativeness, type: Integer, default: 1
  field :conflicthandling, type: Integer, default: 1
  field :learningstyle, type: Integer, default: 1
  field :bondingstyle, type: Integer, default: 1
  field :assertivity, type: Integer, default: 1

  embedded_in :user
end