class Event
	include Mongoid::Document

	field :title, type: String
	field :description, type: String
	field :rating, type: Float
	field :price, type: Integer, default: 0

	has_many :comments, class_name: "Comment", dependent: :destroy
  has_many :ratings, as: :ratable
	belongs_to :partner

	has_and_belongs_to_many :user

	def long_name
		return partner.name + " - " + title
	end

  def price_in_credit
		price / 100.0
	end
end