class Quests::Task
  include Mongoid::Document

  field :description, type: String
  field :completed, type: Boolean, default: false

  embedded_in :quest
  belongs_to :event, optional: true

  def completed?
    completed
  end

  def to_react_data
    evt = event ? {event_title: event.title, event_id: event._id.to_s} : {}
    evt.merge({id: _id.to_s, description: description, completed: completed})
  end
end