class Quests::Quest
  include Mongoid::Document

  field :title, type: String, default: ""
  field :description, type: String, default: ""

  embeds_many :tasks, class_name: "Quests::Task"

  has_and_belongs_to_many :dependencies, class_name: "Quests::Quest", inverse_of: :depends_on_me
  has_and_belongs_to_many :depends_on_me, class_name: "Quests::Quest", inverse_of: :dependencies

  accepts_nested_attributes_for :tasks, :dependencies, allow_destroy: true

  def to_react_data
    {id: _id.to_s, title: title, description: description, tasks: tasks.map(&:to_react_data),
     dependencies: dependencies.map{|d| {id: d._id.to_s, title: d.title}}}
  end
end
