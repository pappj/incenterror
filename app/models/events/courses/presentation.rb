class Events::Courses::Presentation < Events::Presentation
	belongs_to :course, class_name: "Events::Course"

	def partner
		course.partner
	end
end