class Events::Payment
  attr_reader :user
  attr_reader :amount

  def initialize(user, amount)
    @user = user
    @amount = amount
  end

  def pay
    if @user.credit < amount
      raise PsyCraftErrors::NotEnoughCredit
    end

    @user.credit -= amount
    safe_save
  end

  def pay_back
    @user.credit += @amount
    safe_save
  end

  private
    def safe_save
      unless @user.save
        raise PsyCraftErrors::DatabaseError
      end
    end
end