class Events::Presentation < Event
	has_many :occurrences, class_name: "Events::Occurrence", autosave: true, dependent: :destroy

	accepts_nested_attributes_for :occurrences, allow_destroy: true

	def future_occurrences
		occurrences.where(:date.gt => Time.now)
	end
end