class Events::Course < Event
	has_many :presentations, class_name: "Events::Courses::Presentation", dependent: :destroy
	
	accepts_nested_attributes_for :presentations, allow_destroy: true
end