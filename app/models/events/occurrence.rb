class Events::Occurrence
	include Mongoid::Document
	field :date, type: DateTime
	field :location, type: String
	field :finalized, type: Boolean, default: false

	belongs_to :presentation, inverse_of: :occurrences, class_name: "Events::Presentation"
	has_many :participations, class_name: "Events::Participation", dependent: :destroy

	def partner
		presentation.partner
	end

	def event
		presentation
	end
end