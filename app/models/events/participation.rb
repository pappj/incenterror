class Events::Participation
  include Mongoid::Document
  field :status, type: Symbol, default: :going
  field :confirmed, type: Symbol, default: :no

  belongs_to :user
  belongs_to :occurrence

  def username
  	return user.username
  end

  def confirmed?
  	return confirmed == :yes || confirmed == :new
  end
end