class Rating
  include Mongoid::Document

  field :value, type: Integer, default: 3
  field :review, type: String, default: ""

  belongs_to :ratable, polymorphic: true
  belongs_to :user
end