module PsyCraftErrors
  class NotEnoughCredit < StandardError; end
  class DatabaseError < StandardError; end
end