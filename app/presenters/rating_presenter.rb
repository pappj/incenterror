class RatingPresenter < MainPresenter
  attr_reader :rating_aggr

  def initialize(rating_aggr, error="")
    super(error)
    @rating_aggr = rating_aggr
  end
end