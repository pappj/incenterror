class MainPresenter
  attr_reader :error

  protected
    def initialize(error)
      @error = error
    end
end