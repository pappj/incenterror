class EventPresenter < MainPresenter
  attr_reader :event

  def initialize(event, rating_presenter = nil, error = "")
    super(event)
    @event = event
    @rating_presenter = rating_presenter
  end

  def rating
    @rating_presenter.rating_aggr
  end

  def model
    "event"
  end
end