class PartnerPresenter < MainPresenter
  attr_reader :partner
  attr_reader :is_owner

  def initialize(partner, err, is_owner, rating_presenter = nil)
    super(err)
    @partner = partner
    @is_owner = is_owner
    @rating_presenter = rating_presenter
  end

  def event_presenters
    partner.events.map{|e| Object.const_get("#{e._type}Presenter").new(e, "")}
  end

  def rating
    @rating_presenter.rating_aggr
  end
end