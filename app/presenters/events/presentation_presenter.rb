class Events::PresentationPresenter < EventPresenter
  def model
    "presentations"
  end

  def type
    I18n.t("labels.event_types.presentation")
  end
end