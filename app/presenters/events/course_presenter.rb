class Events::CoursePresenter < EventPresenter
  def model
    "courses"
  end

  def type
    I18n.t("labels.event_types.course")
  end
end