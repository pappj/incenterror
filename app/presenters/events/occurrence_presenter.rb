class Events::OccurrencePresenter < MainPresenter
  attr_reader :changed
  attr_reader :occ
  attr_reader :participation
  attr_reader :user

  def initialize(occurrence, changed = :no, error = "", participation = nil, user = nil)
    super(error)
    @changed = changed
    @occ = occurrence
    @participation = participation
    @user = user
  end
end