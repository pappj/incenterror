class Events::Courses::PresentationPresenter < Events::PresentationPresenter
  def model
    "courses/presentations"
  end
end