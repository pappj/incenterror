$(document).on('ready turbolinks:load ajax:success', function(){
    $('.rating-form-star').mouseenter(function(){
        highlightStars($(this));
    });

    $('.rating-form').mouseleave(function(){
        highlightStars($('.rating-form-radio:checked'));
    });
});

var highlightStars = function(starElement){
    starElement.prevAll('.rating-form-star').css('background-position','0 0');
    starElement.css('background-position', '0 0');
    starElement.nextAll('.rating-form-star').css('background-position','0 -27px');
};