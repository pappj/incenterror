class TaskRowEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  onSave = (e) => {
    e.preventDefault();
    this.props.saveChanges(this.state);
  };

  shouldComponentUpdate(nextProps, nextState) {
    return this.props !== nextProps || this.state !== nextState;
  };

  render() {
    return (
      <tr>
        <td>{this.props.index}.</td>
        <td><textarea type="text" name="description" value={this.state.description} onChange={this.onChange} placeholder="Leírás" /></td>
        <td>{this.props.event}</td>
        <td><button onClick={this.onSave}>Mentés</button><button onClick={this.props.toggleEdit}>Mégse</button></td>
      </tr>
    )
  }


}