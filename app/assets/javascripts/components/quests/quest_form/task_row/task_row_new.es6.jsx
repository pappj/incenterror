class TaskRowNew extends React.Component {
  render() {
    return (
      <tr>
        <td>Új</td>
        <td><textarea type="text" name="description" value={this.props.description} onChange={this.props.changeDesc} placeholder="Leírás" /></td>
        <td>{this.props.event}</td>
        <td><button onClick={this.props.addTask}>Hozzáadás</button></td>
      </tr>
    )
  }


}