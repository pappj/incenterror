class TaskRowStatic extends React.Component {
  shouldComponentUpdate(nextProps) {
    return this.props !== nextProps;
  };

  render() {
    return (
      <tr>
        <td>{this.props.index}.</td>
        <td>{this.props.description.slice(0,200)}</td>
        <td>{this.props.event}</td>
        <td>
          <button onClick={this.props.toggleEdit}>Szerk.</button>
          <button onClick={this.props.deleteTask}>X</button>
        </td>
      </tr>
    )
  }
}