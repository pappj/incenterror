class QuestForm extends React.Component {
  constructor(props) {
    super(props);
    let stateTasks = [];
    if(props.tasks) {
      stateTasks = props.tasks.slice();
      for(let task of stateTasks) {
        // This still modifies each task in props.
        task["editing"] = false;
      }
    }

    this.state = Object.assign({}, {title: this.props.title,
                                    description: this.props.description,
                                    tasks: stateTasks,
                                    newTask: QuestForm.defaultNewTask(),
                                    dependencies: props.dependencies,
                                    tasksToDelete: []});
  }

  static defaultNewTask() {
    return {description: ""};
  }

  onChange = (e) => {
    this.setState({[e.target.name]: e.target.value});
  };

  toggleEdit = (e, i) => {
    e.preventDefault();
    const tasks = this.state.tasks.slice();
    tasks[i]["editing"] = !this.state.tasks[i].editing;
    this.setState({tasks: tasks});
  };

  saveTaskChanges = (i, editedTask) => {
    const tasks = this.state.tasks.slice();
    tasks[i]["editing"] = false;
    tasks[i]["description"] = editedTask.description;
    tasks[i]["event"] = editedTask.event;

    this.setState({tasks: tasks});
  };

  deleteTask = (e, i) => {
    e.preventDefault();
    const tasks = this.state.tasks.slice();
    const deletedTask = tasks.splice(i, 1)[0];
    console.log(deletedTask);
    const tasksToDelete = this.state.tasksToDelete.slice();
    if(deletedTask.hasOwnProperty("id")) {
      console.log("Task is marked for delete");
      deletedTask._destroy = true;
      console.log(deletedTask);
      tasksToDelete.push(deletedTask);
      console.log(tasksToDelete);
    }

    this.setState({tasks: tasks, tasksToDelete: tasksToDelete});
  };

  addNewTask = (e) => {
    e.preventDefault();
    const taskToAdd = {
      description: this.state.newTask.description,
      event: this.state.newTask.event,
      editing: false,
    };

    this.setState({tasks: [...this.state.tasks, taskToAdd],
                   newTask: QuestForm.defaultNewTask()});
  };

  changeNewTaskDescription = (e) => {
    const newTask = Object.assign({}, this.state.newTask);
    newTask.description = e.target.value;

    this.setState({newTask: newTask});
  };

  deleteDependency = (e, i) => {
    e.preventDefault();
    const dependencies = this.state.dependencies.slice();
    dependencies.splice(i, 1);

    this.setState({dependencies: dependencies});
  };

  addNewDependency = (e) => {
    e.preventDefault();
    const select = document.getElementById("new_dependency");
    const selectedOption = select.options[select.selectedIndex];

    this.setState({dependencies: [...this.state.dependencies,
                                  {id: selectedOption.value, title: selectedOption.text}]})
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const data = Object.assign({},{quest: {title: this.state.title,
                                   description: this.state.description,
                                   tasks_attributes: this.state.tasks.concat(this.state.tasksToDelete),
                                   dependency_ids: this.state.dependencies.map((d) => d.id)},
                                   authenticity_token: this.props.authenticity_token});
    $.ajax({
      type: this.props.formMethod,
      url: this.props.formUrl,
      data: data,
      success: function(data) {
        window.location = "/deadpool/questManager"
      }
    });
  };

  render() {
    let dependencies = "";
    if(this.state.dependencies.length > 0) {
      dependencies = (
        <table>
          <tbody>
            {this.state.dependencies.map((d, i) => {
              return (
                <tr key={d.id}>
                  <td>{d.title}</td>
                  <td><button onClick={(e) => this.deleteDependency(e, i)}>X</button></td>
                </tr>
              )}
            )}
          </tbody>
        </table>
      );
    }

    let newDependency = <p>Még nincsenek küdetések, amit hozzáadhatnál.</p>;
    if(this.props.all_quests.length > 0) {
      newDependency = (
        <div>
          <label>Új előkövetelmény</label>
          <select id="new_dependency">
            {this.props.all_quests.map((q) => {
              return <option value={q.id} key={q.id}>{q.title}</option>;
            })}
          </select>
          <button onClick={(e) => this.addNewDependency(e)}>Hozzáadás</button>
        </div>
      );
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <input type="hidden" name="authenticity_token" value={this.props.authenticity_token} />
        <label>Cím</label>
        <input type="text" id="title" name="title"
               value={this.state.title}
               onChange={this.onChange} />
        <label>Leírás</label>
        <input type="textarea" id="description" name="description"
               value={this.state.description}
               onChange={this.onChange} />

        <h2>Feladatok</h2>
        <table>
          <thead>
            <tr>
              <th />
              <th>Leírás</th>
              <th>Esemény</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.state.tasks.map( (task, i) => {
              if(task.editing) {
                return <TaskRowEdit key={i}
                                    {...task}
                                    saveChanges={(params) => this.saveTaskChanges(i, params)}
                                    toggleEdit={(e) => this.toggleEdit(e, i)} index={i+1} />
              } else {
                return <TaskRowStatic key={i}
                                      {...task}
                                      toggleEdit={(e) => this.toggleEdit(e, i)}
                                      deleteTask={(e) => this.deleteTask(e, i)}
                                      index={i+1} />
              }
            })}
            <TaskRowNew key="newTaskForm"
                        addTask={(e) => this.addNewTask(e)}
                        changeDesc={(e) => this.changeNewTaskDescription(e)}
                        description={this.state.newTask.description} />
          </tbody>
        </table>

        <h2>Előkövetelmények</h2>

        {dependencies}

        {newDependency}

        <input type="submit" value="Mentés" />
      </form>
    );
  }
}